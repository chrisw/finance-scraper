'use strict';

var chai = require('chai');
var expect = chai.expect;
var proxyquire = require('proxyquire');

var Promise = require('bluebird');

var fs = require('fs');

var urls = {
  // Nasdaq urls
  summary: 'http://finance.yahoo.com/q/?s=AAPL',
  keyStats: 'http://finance.yahoo.com/q/ks?s=AAPL',
  profile: 'http://finance.yahoo.com/q/pr?s=AAPL',
  // Bad urls
  badSummary: 'http://finance.yahoo.com/q/?s=APPL'
};

var html = {};

function requestStub(url) {
  var body = html[url];
  return Promise.resolve([null, body]);
}

var bluebirdStub = {
  promisify: promisify
};

function promisify(func) {
  return func;
}

var tickerSymbol = 'AAPL';
var badTickerSymbol = 'APPL';

var correctDescription = 'Apple Inc. designs, manufactures, and markets mobile communication and media devices, personal computers, watches, and portable digital music players worldwide. The company also sells related software, services, accessories, networking solutions, and third-party digital content and applications. It offers iPhone, a line of smartphones that comprise a phone, music player, and Internet device; iPad, a line of multi-purpose tablets; Mac, a line of desktop and portable personal computers; iPod, a line of portable digital music and media players, such as iPod touch, iPod nano, and iPod shuffle; and Apple Watches, personal electronic devices that combine watch technology with an iOS-based user interface. The company also provides iTunes app and the iTunes Store; Mac App Store that allows customers to discover, download, and install Mac applications; iCloud, a cloud service; Apple Pay for making mobile payments; Apple TV, a portfolio of consumer and professional software applications; iOS and OS X operating systems software; iLife, a consumer-oriented digital lifestyle software application suite; iWork, an integrated productivity suite designed to help users create, present, and publish documents, presentations, and spreadsheets; and other application software, including Final Cut Pro, Logic Pro X, and its FileMaker Pro database software. In addition, it offers various Apple-branded and third-party Mac-compatible and iOS-compatible accessories, including headphones, cases, displays, storage devices, and various other connectivity and computing products and supplies. The company sells and delivers digital content and applications through the iTunes Store, App Store, iBooks Store, and Mac App Store; and sells its products through its retail stores, online stores, and direct sales force, as well as through third-party cellular network carriers, wholesalers, retailers, and value-added resellers. The company was founded in 1977 and is headquartered in Cupertino, California.'

var correctMetrics = {
  peRatio: '15.23',
  marketCap: '706.13B',
  dividend: '2.08',
  beta: '1.13',
  volume: '60,445,523',
  nextEarningDate: '21-Jul-15',
  averageVolume: '44,699,600',
  esp: '8.05',
  pegRatio: '1.00',
  roa: '17.12%',
  roe: '38.37%'
};

describe('FinanceScraper', function() {

  var FinanceScraper;

  before(function() {
    html[urls.summary] = fs.readFileSync('test/mocks/summary.html', 'utf8');
    html[urls.keyStats] = fs.readFileSync('test/mocks/keyStats.html', 'utf8');
    html[urls.profile] = fs.readFileSync('test/mocks/profile.html', 'utf8');

    html[urls.badSummary] = fs.readFileSync('test/mocks/badSummary.html', 'utf8');

    var financeScraperProxy = {
      'request': requestStub,
      'bluebird': bluebirdStub
    };

    FinanceScraper = proxyquire('../lib/financeScraper', financeScraperProxy);
  });

  describe('.getTickerData', function() {

    context('promises', function() {

      it('should return the ticker data for the specified stock', function() {
        return FinanceScraper.getTickerData(tickerSymbol)
          .then(onGet)

        function onGet(data) {
          for(var key in data) {
            var actual = data[key];
            var expected = correctMetrics[key];

            expect(actual).to.equal(expected);
          }
        }
      });

      it('should return an error if an unenabled exchange is asked for', function() {
        return FinanceScraper.getTickerData(badTickerSymbol)
          .catch(onError);

        function onError(err) {
          expect(err.message).to.equal('exchange not enabled');
        }
      });

    });

    context('callbacks', function() {

      it('should return the ticker data for the specified stock', function(done) {
        FinanceScraper.getTickerData(tickerSymbol, onGet);

        function onGet(err, data) {
          expect(err).to.not.be.ok;

          for(var key in data) {
            var actual = data[key];
            var expected = correctMetrics[key];

            expect(actual).to.equal(expected);
          }

          done();
        }
      });

      it('should return an error if an unenabled exchange is asked for', function(done) {
        FinanceScraper.getTickerData(badTickerSymbol, onError);

        function onError(err, data) {
          expect(err).to.be.ok;
          expect(err.message).to.equal('exchange not enabled');

          done();
        }
      });

    });

  });

  describe('.getProfileData', function() {

    context('promises', function() {

      it('should return the description of the specified company', function() {
        return FinanceScraper.getProfileData(tickerSymbol)
          .then(onGet);

        function onGet(data) {
          expect(data.description).to.equal(correctDescription);
        }
      });

      it('should return an error if an unenabled exchange is asked for', function() {
        return FinanceScraper.getProfileData(badTickerSymbol)
          .catch(onError);

        function onError(err) {
          expect(err.message).to.equal('exchange not enabled');
        }
      });

    });

    context('callbacks', function() {

      it('should return the description of the specified company', function(done) {
        FinanceScraper.getProfileData(tickerSymbol, onGet)

        function onGet(err, data) {
          expect(err).to.not.be.ok;
          expect(data.description).to.equal(correctDescription);

          done();
        }
      });

      it('should return an error if an unenabled exchange is asked for', function(done) {
        FinanceScraper.getProfileData(badTickerSymbol, onError);

        function onError(err, data) {
          expect(err).to.be.ok;
          expect(err.message).to.equal('exchange not enabled');

          done();
        }
      });

    });

  });

});
