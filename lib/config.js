'use strict';

var config = {
  exchanges: ['-NasdaqGS'],
  urls: {
    exchange: 'http://finance.yahoo.com/q/?s=',
    summary: 'http://finance.yahoo.com/q/?s=',
    keyStats: 'http://finance.yahoo.com/q/ks?s=',
    profile: 'http://finance.yahoo.com/q/pr?s='
  },
  htmlPaths: {
    exchange: '.rtq_exch',
    metrics: '.yfnc_tabledata1',
    description: '.yfnc_modtitlew1 > p'
  },
  exchangeMapping: {
    exchange: 0
  },
  metricMappings: {
    '-NasdaqGS': {
      summary: {
        volume: 9,
        nextEarningDate: 6,
        averageVolume: 10,
      },
      keyStats: {
        marketCap: 0,
        peRatio: 2,
        pegRatio: 4,
        roa: 13,
        roe: 14,
        esp: 21,
        beta: 31,
        dividend: 48
      }
    }
  },
  profileMapping: {
    '-NasdaqGS': {
      description: 0
    }
  }
};

module.exports = config;
