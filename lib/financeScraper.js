/**
 * @module financeScraper
 */

'use strict';

var request = require('request');
var cheerio = require('cheerio');

var R = require('ramda');
var merge = require('merge');

var Promise = require('bluebird');
request = Promise.promisify(request);

var config = require('./config');

var FinanceScraper = {
  getTickerData: getTickerData,
  getProfileData: getProfileData
};

/**
 * Retrieve ticker data for the specified stock
 * .getTickerData
 * @param {string} tickerSymbol
 * @param {function} callback (optional)
 * @returns {promise}
 */
function getTickerData(tickerSymbol, callback) {
  return getExchange(tickerSymbol)
    .then(getData)
    .nodeify(callback);

  function getData(exchange) {
    if(config.exchanges.indexOf(exchange) === -1) {
      return Promise.reject(new Error('exchange not enabled'));
    }

    return Promise.join(
      getSummaryData(tickerSymbol, exchange),
      getKeyStatsData(tickerSymbol, exchange),
      merge);
  }
}

/**
 * Retrieve summary ticker data
 * .getSummaryData
 * @param {string} tickerSymbol
 * @param {string} exchange
 * @returns {promise}
 */
function getSummaryData(tickerSymbol, exchange) {
  var requestURL = config.urls.summary + tickerSymbol;
  var scraper = R.curry(metricScraper);
  var metricMapping = config.metricMappings[exchange].summary;

  return scrapeHTML(requestURL, scraper(metricMapping));
}

/**
 * Retrieve key statistics ticker data
 * .getKeyStatsData
 * @param {string} tickerSymbol
 * @param {string} exchange
 * @returns {promise}
 */
function getKeyStatsData(tickerSymbol, exchange) {
  var requestURL = config.urls.keyStats + tickerSymbol;
  var scraper = R.curry(metricScraper);
  var metricMapping = config.metricMappings[exchange].keyStats;

  return scrapeHTML(requestURL, scraper(metricMapping));
}

/**
 * Retrieve profile data for the specified stock/corportation
 * .getProfileData
 * @param {string} tickerSymbol
 * @param {function} callback (optional)
 * @returns {promise}
 */
function getProfileData(tickerSymbol, callback) {
  return getExchange(tickerSymbol)
    .then(getData)
    .nodeify(callback);

  function getData(exchange) {
    if(config.exchanges.indexOf(exchange) === -1) {
      return Promise.reject(new Error('exchange not enabled'));
    }

    var requestURL = config.urls.profile + tickerSymbol;
    var scraper = R.curry(profileScraper);
    var profileMapping = config.profileMapping[exchange];

    return scrapeHTML(requestURL, scraper(profileMapping));
  }
}

/**
 * Retrieve exchange for a given ticker symbol
 * @param {string} tickerSymbol
 * @returns {promise}
 */
function getExchange(tickerSymbol) {
  var requestURL = config.urls.exchange + tickerSymbol;
  var scraper = R.curry(exchangeScraper);

  return scrapeHTML(requestURL, scraper);
}

/**
 * Scrapes HTML using the finction provided
 * .scrapeHTML
 * @param {string} requestURL
 * @param {function} scraper
 * @returns {promise}
 */
function scrapeHTML(requestURL, scraper) {

  // promisified request returns an array = [response, html]
  return request(requestURL)
    .then(R.last)
    .then(cheerio.load)
    .then(scraper);
}

/**
 * Scrape HTML for the exchange
 * .exchangeScraper
 * @param {object} $
 * @returns {string}
 */
function exchangeScraper($) {
  var htmlPath = config.htmlPaths.exchange;
  var data = $(htmlPath);

  var index = config.exchangeMapping.exchange;
  var exchange = $(data[index]).text();

  return exchange.trim();
}

/**
 * Scrape HTML for the desired metrics
 * .metricScraper
 * @param {object} metricMapping
 * @param {object} $
 * @returns {object}
 */
function metricScraper(metricMapping, $) {
  var metrics = {};

  var htmlPath = config.htmlPaths.metrics;
  var data = $(htmlPath);

  for(var key in metricMapping) {
    var index = metricMapping[key];
    var value = data[index] ? $(data[index]).text() : 'N/A';

    metrics[key] = value;
  }

  return metrics;
}

/**
 * Scrape profile
 * ._profileScraper
 * @param {object} profileMapping
 * @param {object} $
 * @returns {object}
 */
function profileScraper(profileMapping, $) {
  var htmlPath = config.htmlPaths.description;
  var data = $(htmlPath);

  var index = profileMapping.description;
  var description = data[index] ? $(data[index]).text() : 'N/A';

  return { description: description };
}

module.exports = FinanceScraper;
