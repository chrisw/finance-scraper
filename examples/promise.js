'use strict';

var scraper = require('..');

scraper.getTickerData('AAPL')
  .then(onGet)
  .catch(onError);

function onGet(data) {
  console.log(JSON.stringify(data, null, 4));
}

function onError(err) {

  // this should never run
  console.log(err.message);
}
