'use strict';

var scraper = require('..');

scraper.getTickerData('AAPL', function(err, data) {
  if(err) {

    // this should never run
    return console.log(err.message);
  }

  console.log(JSON.stringify(data, null, 4));
});
