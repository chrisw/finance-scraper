# Introduction

```finance-scraper``` is a scraper written to pull ticker and profile data from Yahoo Finance.

Both callback and promise([Bluebird](https://github.com/petkaantonov/bluebird/blob/master/README.md)) styles are supported

# Metrics
- Market Cap
- Volume
- Average Volume
- Dividend
- Beta
- PE Ratio
- PEG Ratio
- PSE
- ROA
- ROE
- Next Earning Date

# Profile
- Description

# Installation

    npm install finance-scraper
    
# Usage

### Ticker Data

```js
var financeScraper = require('finance-scraper');

financeScraper.getTickerData('AAPL')
    .then(onGet);

function onGet(data) {
    console.log('Beta: ', data.beta);
    console.log('Market Cap: ', data.marketCap);
    ...
}
```

### Profile Data

```js
var financeScraper = require('finance-scraper');

financeScraper.getProfileData('AAPL')
    .then(onGet);

function onGet(data) {
    console.log('Description: ', data.description);
    ...
}
```